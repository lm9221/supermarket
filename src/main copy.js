import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'

// 引入ElementUI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI);

// 使用全局配置axios，只有一台服务器情况下
// Vue.prototype.$axios = axios
// axios.defaults.baseURL="http://123.207.32.32:8000"


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

//#region 
// axios
// axios({
//   url: 'http://123.207.32.32:8000/home/multidata',
// }).then(res => {
//   console.log(res);
// })

// axios.get('http://123.207.32.32:8000/home/multidata', {
//     params: {
//       type: 'pop',
//       page: 1
//     }
//   })
//   .then(res => {
//     console.log(res);
//   })

// 发送并发请求
// axios.all([
//   axios({
//     url: 'http://123.207.32.32:8000/home/multidata'
//   }),
//   axios({
//     url: 'http://123.207.32.32:8000/home/data',
//     params: {
//       type: 'sell',
//       page: 5
//     }
//   })
// ]).then(axios.spread((res1, res2) => {
//   console.log(res1);
//   console.log(res2);
// }))
//#endregion 

// axios实例
// const instancel = axios.create({
//   baseURL: 'http://123.207.32.32:8000',
//   timeout: 5000
// })

// instancel({
//   url: '/home/multidata'
// }).then(res => {
//   console.log(res);
// })

// const instance2 = axios.create({
//   baseURL: 'http://123.207.32.32:8000',
//   timeout: 5000
// })

// instance2({
//   baseURL: 'http://123.207.32.32:8000',
//   timeout: 5000
// })