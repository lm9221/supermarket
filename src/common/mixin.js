import { debounce } from '@/common/utils'
import BackTop from '@/components/content/backTop/BackTop'

export const itemListenerMixin = {
  data() {
    return {
      imgItemListener: null, //控制商品图片加载显示
    }
  },
  mounted() {
    // 监听事件总线中商品图片全部加载完成
    // 运用函数防抖动
    const newrefresh = debounce(this.$refs.scroll.refresh, 100)
    this.imgItemListener = () => {
      newrefresh()
    }
    this.$bus.$on('itemImgLoad', this.imgItemListener)
  },
}

// 返回顶部
export const backTopMixin = {
  data() {
    return {
      isShowbackTop: false, //默认返回顶部按钮隐藏
    }
  },
  components: {
    BackTop,
  },
  methods: {
    backClick() {
      // console.log(this.$refs.scroll);
      this.$refs.scroll.scrollTo(0, 0, 600)
    },
    listenShoBacktop(position) {
      this.isShowbackTop = -position.y > 1000
    },
  },
}
