import Vue from 'vue'
import App from './App.vue'
import store from './store'
// 引入VueRouter
import VueRouter from 'vue-router'
// 引入路由器
import router from './router'

// 使用vantui的Toast
import { Toast,Icon } from 'vant'
import 'vant/lib/icon/local.css';
Vue.use(Toast)
Vue.use(Icon)


// 引入图片懒加载
import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload, {
  preLoad: 1.3,
  loading: require('./assets/img/loading.gif'),
  attempt: 3
})

// 应用
Vue.use(VueRouter)
// 引入ElementUI
// import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'
// Vue.use(ElementUI);

// 使用全局配置axios，只有一台服务器情况下
// Vue.prototype.$axios = axios
// axios.defaults.baseURL="http://123.207.32.32:8000"

router.beforeEach((to, from, next)=>{
  if(to.meta.title){
    document.title = to.meta.title
  }
  next()
})

Vue.filter('filterPrice', function (val) {
  return '￥' + Number(val).toFixed(2)
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  beforeCreate() {
    Vue.prototype.$bus = this //安装全局事件总线，$bus就是当前应用的vm
  },
}).$mount('#app')