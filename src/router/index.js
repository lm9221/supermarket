import Router from 'vue-router';

// 在VUE中路由遇到Error: Avoided redundant navigation to current location:报错显示是路由重复
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
}
// Vue.use(Router);

const Home = () => import('@/views/home/Home.vue');
const Category = () => import('@/views/category/Category.vue');
const Cart = () => import('@/views/cart/Cart.vue');
const Profile = () => import('@/views/profile/Profile.vue');
const Detail = () => import('@/views/detail/Detail.vue');


const router = new Router({
  routes: [{
      path: '',
      redirect: '/home'
    },
    {
      path: '/home',
      component: Home,
      meta: {
        title: '首页'
      }
    },
    {
      path: '/category',
      component: Category,
      meta: {
        title: '分类'
      }
    },
    {
      path: '/cart',
      component: Cart,
      meta: {
        title: '购物车'
      }
    },
    {
      path: '/profile',
      component: Profile,
      meta: {
        title: '个人中心'
      }
    },
    {
      name: 'Detial',
      path: '/detail/:id',
      component: Detail,
      meta: {
        title: '商品详情'
      }
    }
  ]
});

export default router;