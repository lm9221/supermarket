import {
  request
} from './request'

//首页轮播图及nav导航
export function getHomeMultidata() {
  return request({
    url: '/home/multidata'
  })
}

//分页请求
export function getHomeGoods(type, page) {
  return request({
    url: '/home/data',
    params: {
      type,
      page
    }
  })
}