import Vue from 'vue'
import Vuex from 'vuex'
import { Toast } from 'vant'

Vue.use(Vuex)

//储存数据
const state = {
  // JSON.parse将JSON字符串转换成对象
  cartList: JSON.parse(localStorage.getItem('cartList')) || [], //储存购物车添加的商品数据
}
//加工actions传过来的数据，相对应更新state数据
//尽量让其执行较单一的业务逻辑
const mutations = {
  // 添加购物车
  ADDTOCART(state, payload) {
    // JSON.stringify转换成JSON字符串
    state.cartList.push(payload)
    localStorage.setItem('cartList', JSON.stringify(state.cartList))
  },
  // 购物车商品数量加、减
  SubTract(state, payload) {
    state.cartList.forEach((item) => {
      if (item.id === payload.id) {
        if (item.num > 1) {
          item.num--
        } else {
          return item.num
        }
      }
    })
  },

  GoodsAdd(state, payload) {
    state.cartList.forEach((item) => {
      if (item.id === payload.id) {
        item.num++
      }
    })
  },
  //购物车选中商品/单选
  CheckTodo(state, id) {
    state.cartList.forEach((item) => {
      if (item.id === id) item.done = !item.done
    })
  },
  //全选or取消全选商品
  AllCheckTodo(state, val) {
    state.cartList.forEach((item) => (item.done = val))
  },
  //删除选中商品
  GoodsDel(state) {
    state.cartList = state.cartList.filter((item) => {
      return item.done != true
    })
    localStorage.setItem('cartList', JSON.stringify(state.cartList))
    Toast('删除成功！')
  },
}
//计算进一步加工state数据
const getters = {
  //计算在购物车中的商品总数
  getTotal(state) {
    // 方法一
    // let total = 0
    // state.cartList.forEach(item => total += 1);
    // return total
    // 方法二
    let len = state.cartList.length
    return len
  },
  //计算购物车选中商品总价格
  getPriceTotal(state) {
    // 方法一 求商品总价格 使用reduce高阶函数
    let total = state.cartList
      .filter((item) => {
        return item.done
      })
      .reduce((preValue, item) => {
        return preValue + Number(item.price) * item.num
      }, 0)
    return total
    // 方法二
    // let total = 0
    // state.cartList.forEach(item => {
    //   if (item.done) {
    //     total += (Number(item.price) * item.num)
    //   }
    // })
    // return total
  },
  // 计算购物车选中商品数量
  getShopSum(state) {
    // 方法二
    let arrNum = state.cartList.filter((item) => {
      return item.done
    })
    return arrNum.length
    // return arrNum
    // 方法一
    // let total = 0
    // state.cartList.forEach(item => {
    //   if (item.done) {
    //     total += 1
    //   }
    // })
    // return total
  },
}
//异步操作，复杂逻辑尽量在actions完成
const actions = {
  //添加购物车、判断商品是否存在
  goodsAdd(context, payload) {
    let oldProduct = context.state.cartList.find((item) => {
      return item.id === payload.id
    })
    // console.log(oldProduct);
    if (!oldProduct) {
      payload.num = 1
      payload.done = false
      context.commit('ADDTOCART', payload)
      Toast.success('添加成功！')
    } else {
      Toast.fail('已存在该商品!')
    }
  },
  // 接收点击按钮传过来的布尔值来控制全选or取消全选开关
  selectAll(context, val) {
    context.commit('AllCheckTodo', val)
  },
}

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
})
